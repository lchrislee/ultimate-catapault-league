// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "BigBallPowerup.h"
#include "Ball.h"
#include "CatapultLeaguePawn.h"
#include "CatapultLeagueGameMode.h"


ABigBallPowerup::ABigBallPowerup() : Super() {
	if (SphereComponent != nullptr)
	{
		SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ABigBallPowerup::OnHit);
	}

	Label->SetText(FText::FromString(TEXT("Big Ball Powerup")));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialGet(TEXT("/Game/VehicleAdv/Materials/MaterialInstances/SolidOrange.SolidOrange"));
	SphereVisual->CreateDynamicMaterialInstance(0, MaterialGet.Object);

}

void ABigBallPowerup::BeginPlay() {
	Super::BeginPlay();
}

void ABigBallPowerup::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr)
	{
		/* if it's a car, we trigger */
		if (OtherActor->IsA(ACatapultLeaguePawn::StaticClass()))
		{
			ACatapultLeagueGameMode* gameMode = Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode());
			ABall* ball = gameMode->GetBall();
			if (ball)
			{
				ball->Grow();
				SetActorEnableCollision(false);
				this->Destroy();
			}
		}
	}
}