// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "MovingPlatform.h"


// Sets default values
AMovingPlatform::AMovingPlatform()
	: InitialDelay(true)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Create and position a mesh component so we can see where our sphere is
	PlatformVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> PlatformVisualAsset(TEXT("/Engine/EngineMeshes/Cube.Cube"));
	if (PlatformVisualAsset.Succeeded())
	{
		PlatformVisual->SetStaticMesh(PlatformVisualAsset.Object);
		PlatformVisual->SetWorldScale3D(FVector(1.0f));
		PlatformVisual->SetAllMassScale(20.0);
	}
	zOffset = 0;
	up = 0;
	everyOther = true;

}

// Called when the game starts or when spawned
void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMovingPlatform::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if (InitialDelay)
	{
		return;
	}
	everyOther = !everyOther;
	if (everyOther)
	{
		FVector location = GetActorLocation();

		if (zOffset == 30)
		{
			up = 1;
		}
		if (zOffset == 0)
		{
			up = 0;
		}
		if (up == 0)
		{
			zOffset++;
			location.Z = location.Z + zOffset;
		}
		else
		{
			location.Z = location.Z - zOffset;
			zOffset--;
		}
		SetActorLocation(location);
	}
	

}

void AMovingPlatform::Delay(float amount)
{
	GetWorld()->GetTimerManager().SetTimer(Handle, this, &AMovingPlatform::StartMovement, amount);
}

void AMovingPlatform::StartMovement()
{
	InitialDelay = false;
}

