// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Vehicles/VehicleWheel.h"
#include "CatapultLeagueWheelRear.generated.h"

UCLASS()
class UCatapultLeagueWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UCatapultLeagueWheelRear();
};



