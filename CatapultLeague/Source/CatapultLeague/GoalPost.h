// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GoalPost.generated.h"

UCLASS()
class CATAPULTLEAGUE_API AGoalPost : public AActor
{
	GENERATED_BODY()
    
	
public:	
	// Sets default values for this actor's properties
	AGoalPost();
	void Scale(float scale);
    void TranslateX(float units);
    void TranslateY(float units);
    void TranslateZ(float units);
    void GrowAndShrink(float time);
	void ShrinkAndGrow(float time);
    void ScaleToDefault();
    void TranslateToXInTime(float translation, float time);
    void TranslateToYInTime(float translation, float time);
    void TranslateToZInTime(float translation, float time);

	UFUNCTION()
	void OnLowerHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnUpperHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	void SetTeamNumber(int i) { TeamNumber = i; }
	int GetTeamNumber() { return TeamNumber; }

	void SetScale(int s) { scale = s; }
	float GetScale() { return scale; }

private:
    UPROPERTY(EditAnywhere)
    class UStaticMeshComponent* GoalPostMeshComponent;
	class UBoxComponent* GoalPostLowerBoxComponent;
	class UBoxComponent* GoalPostUpperBoxComponent;
	int TeamNumber;
	float scale;
	FTimerHandle GrowHandle;
	FTimerHandle ShrinkHandle;
	
};
