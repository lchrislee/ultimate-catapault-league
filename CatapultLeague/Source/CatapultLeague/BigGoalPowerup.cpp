// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "BigGoalPowerup.h"
#include "GoalPost.h"
#include "CatapultLeagueGameMode.h"
#include "CatapultLeaguePawn.h"

ABigGoalPowerup::ABigGoalPowerup()
	:Super()
{
	if (SphereComponent != nullptr)
	{
		SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ABigGoalPowerup::OnHit);
	}

	Label->SetText(FText::FromString(TEXT("Big Goal Powerup")));


	SphereVisual->SetMobility(EComponentMobility::Movable);
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialGet(TEXT("/Game/VehicleAdv/Materials/MaterialInstances/SolidBrown.SolidBrown"));
	SphereVisual->CreateDynamicMaterialInstance(0, MaterialGet.Object);
}

void ABigGoalPowerup::BeginPlay() {
	Super::BeginPlay();
}


void ABigGoalPowerup::OnHit(UPrimitiveComponent* OverlappedComp,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr)
	{
		/* if it's a car, we trigger */
		if (OtherActor->IsA(ACatapultLeaguePawn::StaticClass()))
		{
			ACatapultLeagueGameMode* gameMode = Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode());
			AGoalPost* goalPostA = gameMode->GetGoalPostA();
			if (goalPostA)
			{
				goalPostA->GrowAndShrink(5.0f);
				SetActorEnableCollision(false);
				this->Destroy();
			}
		}
	}
}



