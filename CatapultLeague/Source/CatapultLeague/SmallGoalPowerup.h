// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Powerup.h"
#include "SmallGoalPowerup.generated.h"

/**
 * 
 */
UCLASS()
class CATAPULTLEAGUE_API ASmallGoalPowerup : public APowerup
{
	GENERATED_BODY()
	
public:
	ASmallGoalPowerup();
	void BeginPlay() override;
	UFUNCTION()
	void OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	
	
	
};
