// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "Powerup.h"
#include "CatapultLeagueGameMode.h"
#include "CatapultLeaguePawn.h"

// Sets default values
APowerup::APowerup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    // Our root component will be a sphere that reacts to physics
    SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
    RootComponent = SphereComponent;
    SphereComponent->InitSphereRadius(35.0f);
    
    // Create and position a mesh component so we can see where our sphere is
    SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PowerupVisualRepresentation"));
    SphereVisual->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Engine/EngineMeshes/Sphere.Sphere"));
    if (SphereVisualAsset.Succeeded())
    {
        SphereVisual->SetStaticMesh(SphereVisualAsset.Object);
        SphereVisual->SetWorldScale3D(FVector(0.15f));
    }
    
    SetActorEnableCollision(true);
    
    SphereComponent->bGenerateOverlapEvents = true;


	Label = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Powerup Label"));
	Label->SetupAttachment(RootComponent);
	Label->SetRelativeLocation(FVector(0.0f, 75.0f, 20.0f));
	Label->SetTextRenderColor(FColor::White);
}

// Called when the game starts or when spawned
void APowerup::BeginPlay()
{
	Super::BeginPlay();
    SetActorScale3D(FVector(1.0f));
    FVector actorLocation = GetActorLocation();
    actorLocation.Z += 20.0f;
    SetActorLocation(actorLocation, false);

}

// Called every frame
void APowerup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	

	ACatapultLeaguePawn* pawn = FindClosestPlayer();
	if (pawn) {
		/* rotate the powerup towards it*/

		FVector loc = pawn->GetActorLocation() - GetActorLocation();



		FRotator NewControlRotation = loc.Rotation();

		NewControlRotation.Yaw = FRotator::ClampAxis(NewControlRotation.Yaw);

		SetActorRotation(NewControlRotation);

	}
	
}

ACatapultLeaguePawn* APowerup::FindClosestPlayer() {
	/* get all players*/
	TArray<AController*> players = Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode())->GetPlayers();
	/* find closest one*/
	ACatapultLeaguePawn* closestPlayer = nullptr;
	float minDist;

	if (players.Num() > 0) {
		closestPlayer = Cast<ACatapultLeaguePawn>((*players.GetData())->GetPawn());
		minDist = (*players.GetData())->GetPawn()->GetDistanceTo(this);
		for (auto i : players) {
			float dist = i->GetPawn()->GetDistanceTo(this);
			if (dist < minDist) {
				minDist = dist;
				closestPlayer = Cast<ACatapultLeaguePawn>(i->GetPawn());
			}
		}
	}

	return closestPlayer;
}


