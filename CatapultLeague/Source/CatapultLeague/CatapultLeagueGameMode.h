// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "CatapultLeagueGameMode.generated.h"

UCLASS(minimalapi)
class ACatapultLeagueGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ACatapultLeagueGameMode();
	
	void BeginPlay() override;
	AActor* ChoosePlayerStart_Implementation(AController* Player) override;
	void UpdateScore(int TeamScoredOn);
	void UpdateTime();
	FText ReturnScore();
	FText ReturnTime();

	const static int NUM_PLAYERS = 4;
	class AGoalPost* GetGoalPostA();
	class AGoalPost* GetGoalPostB();
	class ABall* GetBall() { return Ball;  }
	bool CheckIfGameOver() { return isGameOver; }
	void BeginGameOver();
	void RespawnAll();
	void RespawnPlayer(class ACatapultPlayerController* Player);

	TArray<AController*> GetPlayers() { return Players; }
	static const int STARTING_POWERUP_COUNT = 5;
	static const int NUM_POWERUPS = 5;

private:
	FTimerHandle timer;
	int TimeLeft;

	class ABall* Ball;
	TArray<class APowerup*> PowerUps;
	TArray<class Team*> Teams;
	TArray<class AMovingPlatform*> Platforms;
	bool isGameOver;
	TSet<FVector> Taken;

	void SpawnGoalPosts();
	void SpawnPlatforms();
	void SpawnRandomPowerup();
	void GenerateSpawns();
	TArray<AController*> Players;
	void RespawnBall();
};



