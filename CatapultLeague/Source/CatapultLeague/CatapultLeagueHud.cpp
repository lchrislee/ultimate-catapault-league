// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "CatapultLeague.h"
#include "CatapultLeagueHud.h"
#include "CatapultLeaguePawn.h"
#include "Engine/Canvas.h"
#include "Engine/Font.h"
#include "CanvasItem.h"
#include "CatapultLeagueGameMode.h"

// Needed for VR Headset
#include "Engine.h"
#if HMD_MODULE_INCLUDED
#include "IHeadMountedDisplay.h"
#endif  // HMD_MODULE_INCLUDED
#define LOCTEXT_NAMESPACE "VehicleHUD"

ACatapultLeagueHud::ACatapultLeagueHud()
{
	static ConstructorHelpers::FObjectFinder<UFont> Font(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
	HUDFont = Font.Object;
}

void ACatapultLeagueHud::DrawHUD()
{
	Super::DrawHUD();

	// Calculate ratio from 720p
	const float HUDXRatio = Canvas->SizeX / 1280.f;
	const float HUDYRatio = Canvas->SizeY / 720.f;

	bool bHMDDeviceActive = false;

	if( bHMDDeviceActive == false )
	{
		// Get our vehicle so we can check if we are in car. If we are we don't want onscreen HUD
		ACatapultLeaguePawn* Vehicle = Cast<ACatapultLeaguePawn>(GetOwningPawn());
		FVector2D ScaleVec(HUDYRatio * 1.4f, HUDYRatio * 1.4f);
		FVector2D LargeScaleVec(HUDYRatio * 4.f, HUDYRatio * 4.f);

		if ((Vehicle != nullptr) && (Vehicle->bInCarCameraActive == false))
		{
			if (!Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode())->CheckIfGameOver()){
			FCanvasTextItem ScoreTextItem(FVector2D(HUDXRatio * 600.f, HUDYRatio * 50), Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode())->ReturnScore(), HUDFont, FLinearColor::Green);
			ScoreTextItem.Scale = ScaleVec;
			Canvas->DrawItem(ScoreTextItem);

			FCanvasTextItem TimeTextItem(FVector2D(HUDXRatio * 595.f, HUDYRatio * 105), Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode())->ReturnTime(), HUDFont, FLinearColor::Green);
			TimeTextItem.Scale = ScaleVec;
			Canvas->DrawItem(TimeTextItem);
			}

			else{

				FCanvasTextItem GameOverTextItem(FVector2D(HUDXRatio * 450.f, HUDYRatio * 160), FText::FromString(FString(TEXT("Game Over"))), HUDFont,  FLinearColor::Red);
				GameOverTextItem.Scale = LargeScaleVec;
				Canvas->DrawItem(GameOverTextItem);
			}
		}
	}
}

#undef LOCTEXT_NAMESPACE
