// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "CatapultPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CATAPULTLEAGUE_API ACatapultPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	
private:
	int PlayerNumber;
	int TeamNumber;
	FName forward;
	FName right;
	FName shoot;
	FName flipCar;
	FTimerHandle CarryTimer;

	static int NEXT_PLAYER;

protected:
	void PlayerTick(float DeltaTime) override;
	void SetupInputComponent() override;
	void OnCountdownComplete();
	
public:
	ACatapultPlayerController();

	void SetPlayerNumber(int i) { PlayerNumber = i; }
	int GetPlayerNumber() { return PlayerNumber; }
	void SetTeamNumber(int i) { TeamNumber = i; }
	int GetTeamNumber() { return TeamNumber; }

	void ResetInputComponent();

	/** Handle pressing forwards */
	void MoveForward(float Val);

	/** Handle pressing right */
	void MoveRight(float Val);

	void OnShootPressed();

	int GetNextPlayerNumber();
	void StartCountdown();

	void FlipCar();
};
