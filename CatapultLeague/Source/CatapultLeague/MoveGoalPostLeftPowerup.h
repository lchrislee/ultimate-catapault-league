// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Powerup.h"
#include "MoveGoalPostLeftPowerup.generated.h"

/**
 * 
 */
UCLASS()
class CATAPULTLEAGUE_API AMoveGoalPostLeftPowerup : public APowerup
{
	GENERATED_BODY() 
	AMoveGoalPostLeftPowerup();
	void BeginPlay() override;
	UFUNCTION()
	void OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	
	
	
};
