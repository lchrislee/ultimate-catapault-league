// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "Ball.h"
#include "CatapultLeaguePawn.h"
#include "CatapultPlayerController.h"

// Sets default values
ABall::ABall()
	: OwningCar(nullptr)
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->bGenerateOverlapEvents = true;

	scale = 0.1f;
	CollisionComp->InitSphereRadius(GetSphereRadius());
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &ABall::OnHit);
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionComp->SetSimulatePhysics(true);
	RootComponent = CollisionComp;


	// Create and position a mesh component so we can see where our sphere is
	SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	SphereVisual->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Engine/EngineMeshes/Sphere.Sphere"));
	if (SphereVisualAsset.Succeeded())
	{
		SphereVisual->SetStaticMesh(SphereVisualAsset.Object);
		SphereVisual->SetWorldScale3D(FVector(scale));
		static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialGet(TEXT("/Game/VehicleAdv/Materials/MaterialInstances/SolidHighlighterYellow.SolidHighlighterYellow"));
		SphereVisual->CreateDynamicMaterialInstance(0, MaterialGet.Object);
	}

	actorScale = 1.0f;
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABall::Tick( float DeltaTime )
{
	Super::Tick(DeltaTime);
}

void ABall::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr && OtherComp->IsSimulatingPhysics())
	{
		if (OtherActor->IsA(ACatapultLeaguePawn::StaticClass()))
		{
			ACatapultLeaguePawn* player = Cast<ACatapultLeaguePawn>(OtherActor);
			if (player)
			{
				CollisionComp->SetSimulatePhysics(false);
				FAttachmentTransformRules rules = FAttachmentTransformRules::FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, true);
				AttachToActor(player, rules);
				OwningCar = player;
				FVector location = GetActorLocation();
				location.Z += GetAttachHeight();
				SetActorLocation(location);
				// Now we need to disable everything on the ball and move it on top of the player
				SetActorEnableCollision(false);

				ACatapultPlayerController* Controller = Cast<ACatapultPlayerController>(player->GetController());
				Controller->StartCountdown();
			}
		}	
	}
}

void ABall::Grow()
{
	actorScale *= 2.0f;

	SetActorScale3D(FVector(actorScale));
}

void ABall::Shrink()
{
	actorScale /= 2.0f;
	SetActorScale3D(FVector(actorScale));
}

void ABall::ResetOwningCar()
{
	OwningCar = nullptr;
}

float ABall::GetSphereRadius() {
	return (scale * 10)*20.0f;
}

float ABall::GetAttachHeight() {
	return actorScale*80.0f;
}

void ABall::ResetScale() {
	actorScale = 1.0f;
	SetActorScale3D(FVector(actorScale));
}
