// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Vehicles/VehicleWheel.h"
#include "CatapultLeagueWheelFront.generated.h"

UCLASS()
class UCatapultLeagueWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UCatapultLeagueWheelFront();
};



