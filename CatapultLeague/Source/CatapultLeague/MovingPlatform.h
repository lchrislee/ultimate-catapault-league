// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MovingPlatform.generated.h"

UCLASS()
class CATAPULTLEAGUE_API AMovingPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMovingPlatform();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* PlatformVisual;
	
	void Delay(float amount);

private:
	float zOffset;
	int up;
	bool everyOther;
	bool InitialDelay;
	FTimerHandle Handle;

	void StartMovement();
};
