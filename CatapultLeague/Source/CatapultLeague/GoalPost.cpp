// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "GoalPost.h"
#include "CatapultLeagueGameMode.h"
#include "Ball.h"


// Sets default values
AGoalPost::AGoalPost()
{	
	static int color = 0;
	scale = 0.25f;

    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    // Create and position a mesh component so we can see where our sphere is
    GoalPostMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootComponent"));
	RootComponent = GoalPostMeshComponent;

    static ConstructorHelpers::FObjectFinder<UStaticMesh> GoalPostVisualAsset(TEXT("/Game/Imports/GoalPosts.GoalPosts"));
	if (GoalPostVisualAsset.Succeeded())
	{
		GoalPostMeshComponent->SetStaticMesh(GoalPostVisualAsset.Object);
		GoalPostMeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		GoalPostMeshComponent->SetWorldScale3D(FVector(scale));
		if (color%2 == 0)
		{
			static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialGet(TEXT("/Game/VehicleAdv/Materials/MaterialInstances/SolidHighlighterYellow.SolidHighlighterYellow"));
			GoalPostMeshComponent->CreateDynamicMaterialInstance(0, MaterialGet.Object);
		}
		else
		{
			static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialGet(TEXT("/Game/VehicleAdv/Materials/MaterialInstances/SolidOrange.SolidOrange"));
			GoalPostMeshComponent->CreateDynamicMaterialInstance(0, MaterialGet.Object);
		}
		color++;
	}

	GoalPostLowerBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("LowerBoxComponent"));
	GoalPostLowerBoxComponent->SetupAttachment(RootComponent);
	GoalPostLowerBoxComponent->bGenerateOverlapEvents = true;
	GoalPostLowerBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AGoalPost::OnLowerHit);
	GoalPostLowerBoxComponent->AddLocalOffset(FVector(0, 0, 550));
	GoalPostLowerBoxComponent->SetBoxExtent(FVector(700,30,550), true);

	GoalPostUpperBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("UpperBoxComponent"));
	GoalPostUpperBoxComponent->SetupAttachment(RootComponent);
	GoalPostUpperBoxComponent->bGenerateOverlapEvents = true;
	GoalPostUpperBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AGoalPost::OnUpperHit);
	GoalPostUpperBoxComponent->AddLocalOffset(FVector(0, 0, 1375));
	GoalPostUpperBoxComponent->SetBoxExtent(FVector(700, 30, 200), true);
}

void AGoalPost::OnLowerHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr && OtherComp->IsSimulatingPhysics())
	{
		if (OtherActor->IsA(ABall::StaticClass()))
		{
			ABall* ball = Cast<ABall>(OtherActor);
			if (ball)
			{
				ACatapultLeagueGameMode* Master = Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode());
				Master->UpdateScore(GetTeamNumber());
				Master->RespawnAll();
			}
		}
	}
}

void AGoalPost::OnUpperHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr && OtherComp->IsSimulatingPhysics())
	{
		if (OtherActor->IsA(ABall::StaticClass()))
		{
			ABall* ball = Cast<ABall>(OtherActor);
			if (ball)
			{
				ACatapultLeagueGameMode* Master = Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode());
				Master->UpdateScore(GetTeamNumber());
				Master->UpdateScore(GetTeamNumber());
				Master->RespawnAll();
			}
		}
	}
}

/* grows and shrinks with a waiting period based on the time that was based in */
void AGoalPost::GrowAndShrink(float time){
	scale += 3.0f;
    Scale(scale);
	if (GrowHandle.IsValid()) {
		GetWorldTimerManager().ClearTimer(GrowHandle);
	}
    GetWorldTimerManager().SetTimer(GrowHandle, this, &AGoalPost::ScaleToDefault,time, false);
}

void AGoalPost::ShrinkAndGrow(float time) {
	scale += 0.5f;
	Scale(scale);
	if (ShrinkHandle.IsValid()) {
		GetWorldTimerManager().ClearTimer(ShrinkHandle);
	}
	GetWorldTimerManager().SetTimer(GrowHandle, this, &AGoalPost::ScaleToDefault, time, false);
}

void AGoalPost::TranslateToXInTime(float translation, float time){
    TranslateX(translation);
    FTimerHandle handle;
    FTimerDelegate TimerDelX = FTimerDelegate::CreateUObject( this, &AGoalPost::TranslateX, -translation );
    GetWorldTimerManager().SetTimer(handle, TimerDelX,time, false);
}
void AGoalPost::TranslateToYInTime(float translation, float time){
    TranslateY(translation);
    FTimerHandle handle;
    FTimerDelegate TimerDelY = FTimerDelegate::CreateUObject( this, &AGoalPost::TranslateY, -translation );
    GetWorldTimerManager().SetTimer(handle, TimerDelY,time, false);
}
void AGoalPost::TranslateToZInTime(float translation, float time){
    TranslateZ(translation);
    FTimerHandle handle;
    FTimerDelegate TimerDelZ = FTimerDelegate::CreateUObject( this, &AGoalPost::TranslateZ, -translation );
    GetWorldTimerManager().SetTimer(handle, TimerDelZ,time, false);
}

void AGoalPost::ScaleToDefault(){
    Scale(0.25f);
}

void AGoalPost::Scale(float scale){
    SetActorScale3D(FVector(scale));
}
void AGoalPost::TranslateX(float units){
    FVector actorLocation = GetActorLocation();
    actorLocation.X += units;
    SetActorLocation(actorLocation, false);
}
void AGoalPost::TranslateY(float units){
    FVector actorLocation = GetActorLocation();
    actorLocation.Y += units;
    SetActorLocation(actorLocation, false);
}

void AGoalPost::TranslateZ(float units){
    FVector actorLocation = GetActorLocation();
    actorLocation.Z += units;
    SetActorLocation(actorLocation, false);
}
