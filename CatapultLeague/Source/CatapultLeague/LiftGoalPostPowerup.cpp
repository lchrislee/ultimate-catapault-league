// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "LiftGoalPostPowerup.h"
#include "GoalPost.h"
#include "CatapultLeaguePawn.h"
#include "CatapultLeagueGameMode.h"

ALiftGoalPostPowerup::ALiftGoalPostPowerup() :Super() {
	if (SphereComponent != nullptr)
	{
		SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ALiftGoalPostPowerup::OnHit);
	}

	Label->SetText(FText::FromString(TEXT("Lift Goal Powerup")));

	SphereVisual->SetMobility(EComponentMobility::Movable);
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialGet(TEXT("/Game/VehicleAdv/Materials/MaterialInstances/SolidPink.SolidPink"));
	SphereVisual->CreateDynamicMaterialInstance(0, MaterialGet.Object);
}

void ALiftGoalPostPowerup::BeginPlay() {
	Super::BeginPlay();
}

void ALiftGoalPostPowerup::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr)
	{
		/* if it's a car, we trigger */
		if (OtherActor->IsA(ACatapultLeaguePawn::StaticClass()))
		{
			ACatapultLeagueGameMode* gameMode = Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode());
			AGoalPost* goalPostA = gameMode->GetGoalPostA();
			if (goalPostA)
			{
				goalPostA->TranslateToZInTime(100.0f, 10.0f);
				SetActorEnableCollision(false);
				this->Destroy();
			}
		}
	}
}




