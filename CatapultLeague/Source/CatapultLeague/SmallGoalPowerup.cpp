// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "SmallGoalPowerup.h"
#include "GoalPost.h"
#include "CatapultLeaguePawn.h"
#include "CatapultLeagueGameMode.h"

ASmallGoalPowerup::ASmallGoalPowerup() 
	:Super() 
{
	if (SphereComponent != nullptr)
	{
		SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ASmallGoalPowerup::OnHit);
	}

	Label->SetText(FText::FromString(TEXT("Small Goal Powerup")));

	SphereVisual->SetMobility(EComponentMobility::Movable);
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialGet(TEXT("/Game/VehicleAdv/Materials/MaterialInstances/SolidBlue.SolidBlue"));
	SphereVisual->CreateDynamicMaterialInstance(0, MaterialGet.Object);
}

void ASmallGoalPowerup::BeginPlay()
{
	Super::BeginPlay();
}

void ASmallGoalPowerup::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr)
	{
		/* if it's a car, we trigger */
		if (OtherActor->IsA(ACatapultLeaguePawn::StaticClass()))
		{
			// TODO: we have to determine what team the car is on and apply the powerup to the appropriate team's goal
			ACatapultLeagueGameMode* gameMode = Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode());
			AGoalPost* goalPostA = gameMode->GetGoalPostA();
			if (goalPostA)
			{
				goalPostA->ShrinkAndGrow(5.0f);
				SetActorEnableCollision(false);
				this->Destroy();
			}
		}
	}
}
