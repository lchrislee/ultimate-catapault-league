// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameViewportClient.h"
#include "CatapultGameViewportClient.generated.h"

/**
 * 
 */
UCLASS()
class CATAPULTLEAGUE_API UCatapultGameViewportClient : public UGameViewportClient
{
	GENERATED_BODY()
	
public:
	bool InputKey(FViewport* Viewport, int32 ControllerId, FKey Key, EInputEvent EventType, float AmountDepressed = 1.f, bool bGamepad = false) override;
	
};
