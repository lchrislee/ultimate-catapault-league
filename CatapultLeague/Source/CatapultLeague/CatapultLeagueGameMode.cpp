// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "CatapultLeague.h"
#include "CatapultLeagueGameMode.h"
#include "CatapultLeaguePawn.h"
#include "CatapultLeagueHud.h"
#include "CatapultPlayerController.h"
#include "Ball.h"
#include "GoalPost.h"
#include "Team.h"
#include "MovingPlatform.h"
#include <string>
#include <set>
#include "BigBallPowerup.h"
#include "BigGoalPowerup.h"
#include "LiftGoalPostPowerup.h"
#include "MoveGoalPostLeftPowerup.h"
#include "MoveGoalPostRightPowerup.h"
#include "SmallBallPowerup.h"
#include "SmallGoalPowerup.h"

ACatapultLeagueGameMode::ACatapultLeagueGameMode()
	: TimeLeft(180)
{
	DefaultPawnClass = ACatapultLeaguePawn::StaticClass();
	HUDClass = ACatapultLeagueHud::StaticClass();
	PlayerControllerClass = ACatapultPlayerController::StaticClass();

	Teams.Add(new Team(0));
	Teams.Add(new Team(1));
	isGameOver = false;
}

void ACatapultLeagueGameMode::BeginPlay()
{
	Super::BeginPlay();

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = NULL;
	SpawnInfo.bDeferConstruction = false;
	Players.Add(GetWorld()->GetFirstPlayerController());

	for (int i = 1; i < NUM_PLAYERS; ++i)
	{
		APlayerController* NewPlayer = UGameplayStatics::CreatePlayer(GetWorld(), -1, true);
		ACatapultPlayerController* Player = Cast<ACatapultPlayerController>(NewPlayer);

		Teams[i % 2]->Players.Add(Player);
		Player->SetTeamNumber(i % 2);
		Players.Add(Player);
	}
	
	RespawnBall();
	SpawnPlatforms();
	SpawnGoalPosts();

	for (int i = 0; i < STARTING_POWERUP_COUNT; ++i)
	{
		SpawnRandomPowerup();
	}

	FTimerHandle CountDownTimer;
	GetWorld()->GetTimerManager().SetTimer(CountDownTimer, this, &ACatapultLeagueGameMode::UpdateTime, 1, true);
	FTimerHandle PowerupTimer;
	GetWorld()->GetTimerManager().SetTimer(PowerupTimer, this, &ACatapultLeagueGameMode::SpawnRandomPowerup, 15, true);
}

AActor* ACatapultLeagueGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	Super::ChoosePlayerStart_Implementation(Player);
	GenerateSpawns();
	
	ACatapultPlayerController* controller = Cast<ACatapultPlayerController>(Player);
	if (controller == nullptr)
	{
		return nullptr;
	}

	int Id = controller->GetPlayerNumber();

	TArray<APlayerStart*> Starts = Teams[Id % 2]->PlayerStarts;

	int index;
	FVector location;

	do
	{
		index = rand() % Starts.Num();
		location = Starts[index]->GetActorLocation();
	} while (Taken.Find(location));
	Taken.Add(location);
	return Starts[index];
}

void ACatapultLeagueGameMode::UpdateScore(int TeamScoredOn)
{
	if (TeamScoredOn > Teams.Num() || TeamScoredOn < 0)
	{
		return;
	}

	int TeamToIncrease = Teams.Num() - 1 - TeamScoredOn; // Increment the other team.
	Teams[TeamToIncrease]->Score += 1;
}

void ACatapultLeagueGameMode::UpdateTime()
{
	TimeLeft--;
	if (TimeLeft < 0)
	{
		GetWorld()->GetTimerManager().ClearTimer(timer);
		TimeLeft = 0;
		// TODO : check for overtime possibility, we'd probably just start another quarter - no need to get fancy
		// display text in the middle of the screen that says game over, we disable user input, so that no one can move anymore
		BeginGameOver();
	}
}

FText ACatapultLeagueGameMode::ReturnScore()
{
	
	FString score = FString();
	score += FString::FromInt(Teams[0]->Score);
	score += FString(TEXT(" : "));
	score += FString::FromInt(Teams[1]->Score);
	FText ScoreHUDText = FText::FromString(score);
	return ScoreHUDText;
}

FText ACatapultLeagueGameMode::ReturnTime()
{
	FString time = FString();
	time += FString::FromInt(TimeLeft / 60);
	time += FString(TEXT(" : "));
	if (TimeLeft % 60 < 10)
	{
		time += "0";
	}
	time += FString::FromInt(TimeLeft % 60);
	FText TimeHUDText = FText::FromString(time);
	return TimeHUDText;
}

void ACatapultLeagueGameMode::SpawnGoalPosts()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = NULL;
	SpawnInfo.bDeferConstruction = false;

	Teams[0]->GoalPost = GetWorld()->SpawnActor<AGoalPost>(
		AGoalPost::StaticClass(),
		FVector(-13360.0f, 10280.0f, 10.0f),
		FRotator(0, 90, 0),
		SpawnInfo
		);
	Teams[0]->GoalPost->SetTeamNumber(0);
	
	Teams[1]->GoalPost = GetWorld()->SpawnActor<AGoalPost>(
		AGoalPost::StaticClass(),
		FVector(-5000.0f, 10280.0f, 10.0f),
		FRotator(0, 90, 0),
		SpawnInfo
		);
	Teams[1]->GoalPost->SetTeamNumber(1);
}

void ACatapultLeagueGameMode::SpawnPlatforms()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = NULL;
	SpawnInfo.bDeferConstruction = false;

	for (int i = 0; i < 4; ++i)
	{
		Platforms.Add(GetWorld()->SpawnActor<AMovingPlatform>(
			AMovingPlatform::StaticClass(),
			FVector(-10000.0f + rand() % 1750, 10000.0f + rand() % 3000, -150.0f),
			FRotator::ZeroRotator,
			SpawnInfo
			));
	}
	
	for (int i = 0; i < 4; ++i)
	{
		Platforms.Add(GetWorld()->SpawnActor<AMovingPlatform>(
			AMovingPlatform::StaticClass(),
			FVector(-6000.0f - rand() % 1750, 10000.0f + rand() % 3000, -150.0f),
			FRotator::ZeroRotator,
			SpawnInfo
			));
	}

	for (int i = 0; i < Platforms.Num(); ++i)
	{
		Platforms[i]->Delay(rand() % 2 * i);
	}
}

void ACatapultLeagueGameMode::SpawnRandomPowerup()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = NULL;
	SpawnInfo.bDeferConstruction = false;

	APowerup* Powerup = nullptr;

	FVector location(-10750 + rand() % 3750, 8000 + rand() % 4000, 10);

	switch (rand() % NUM_POWERUPS)
	{
	case 0:
		Powerup = GetWorld()->SpawnActor<ALiftGoalPostPowerup>(
			ALiftGoalPostPowerup::StaticClass(),
			location,
			FRotator::ZeroRotator,
			SpawnInfo
			);
		break;
	case 1:
		Powerup = GetWorld()->SpawnActor<ABigGoalPowerup>(
			ABigGoalPowerup::StaticClass(),
			location,
			FRotator::ZeroRotator,
			SpawnInfo
			);
		break;
	case 2:
		Powerup = GetWorld()->SpawnActor<AMoveGoalPostLeftPowerup>(
			AMoveGoalPostLeftPowerup::StaticClass(),
			location,
			FRotator::ZeroRotator,
			SpawnInfo
			);
		break;
	case 3:
		Powerup = GetWorld()->SpawnActor<AMoveGoalPostRightPowerup>(
			AMoveGoalPostRightPowerup::StaticClass(),
			location,
			FRotator::ZeroRotator,
			SpawnInfo
			);
		break;
	case 4:
		Powerup = GetWorld()->SpawnActor<ASmallGoalPowerup>(
			ASmallGoalPowerup::StaticClass(),
			location,
			FRotator::ZeroRotator,
			SpawnInfo
			);
		break;
	}
}

AGoalPost* ACatapultLeagueGameMode::GetGoalPostA() { return Teams[0]->GoalPost; }
AGoalPost* ACatapultLeagueGameMode::GetGoalPostB() { return Teams[1]->GoalPost; }

void ACatapultLeagueGameMode::GenerateSpawns()
{
	if (Teams[0]->PlayerStarts.Num() > 0)
	{
		return;
	}

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = NULL;
	SpawnInfo.bDeferConstruction = false;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			FVector location(-12750.0f + 175.0f * i, 10000 + 200.0f * j, 20);
			APlayerStart* PlayerStart = Cast<APlayerStart>(
				GetWorld()->SpawnActor<APlayerStart>(
					APlayerStart::StaticClass(),
					location,
					FRotator::ZeroRotator,
					SpawnInfo)
				);
			Teams[0]->PlayerStarts.Add(PlayerStart);
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			FVector location(-5750.0f - (175.0f * i), 10000 + 200.0f * j, 20);
			APlayerStart* PlayerStart = Cast<APlayerStart>(
				GetWorld()->SpawnActor<APlayerStart>(
					APlayerStart::StaticClass(),
					location,
					FRotator(180, 0, 0),
					SpawnInfo)
				);
			Teams[1]->PlayerStarts.Add(PlayerStart);
		}
	}
}

void ACatapultLeagueGameMode::RespawnBall()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = NULL;
	SpawnInfo.bDeferConstruction = false;

	Ball = GetWorld()->SpawnActor<ABall>(
		ABall::StaticClass(),
		FVector(-9120.0f, 10280.0f, 30.0f),
		FRotator::ZeroRotator,
		SpawnInfo
	);
	Ball->GetCollisionComp()->AddImpulse(FVector(0, 0, 20000));
}

void ACatapultLeagueGameMode::BeginGameOver() {
	isGameOver = true;
	GetWorld()->GetFirstPlayerController()->SetIgnoreMoveInput(true);
	for (Team* t : Teams)
	{
		for (APlayerController* player : t->Players) {
			if (player)
			{
				player->SetIgnoreMoveInput(true);
			}
		}
	}
	
}

void ACatapultLeagueGameMode::RespawnAll()
{
	Ball->Destroy();

	Taken.Empty();
	for (Team* t : Teams)
	{
		for (ACatapultPlayerController* p : t->Players)
		{
			RespawnPlayer(p);
		}
	}

	RespawnBall();

	for (APowerup* powerup : PowerUps)
	{
		powerup->Destroy();
	}
}

void ACatapultLeagueGameMode::RespawnPlayer(ACatapultPlayerController * Player)
{
	int TeamNumber = Player->GetTeamNumber() + 1;

	TArray<APlayerStart*> Starts = Teams[TeamNumber % 2]->PlayerStarts;
	
	

	int index;
	FVector location;
	do
	{
		index = rand() % Starts.Num();
		location = Starts[index]->GetActorLocation();
	} while (Taken.Contains(location));
	Taken.Add(location);
	Player->GetPawn()->SetActorLocation(location, false, nullptr, ETeleportType::TeleportPhysics);
	if (TeamNumber == 1) {
		Player->GetPawn()->SetActorRotation(FQuat(0, 0, .5, 0), ETeleportType::TeleportPhysics);
	}

	else {
		Player->GetPawn()->SetActorRotation(FQuat(0, 0, 0, 0), ETeleportType::TeleportPhysics);
	}
		
		
		
	
}
