// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/HUD.h"
#include "CatapultLeagueHud.generated.h"


UCLASS(config = Game)
class ACatapultLeagueHud : public AHUD
{
	GENERATED_BODY()

public:
	ACatapultLeagueHud();

	/** Font used to render the vehicle info */
	UPROPERTY()
	UFont* HUDFont;

	// Begin AHUD interface
	virtual void DrawHUD() override;
	// End AHUD interface

};
