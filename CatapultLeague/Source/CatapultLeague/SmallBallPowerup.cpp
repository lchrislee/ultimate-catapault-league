// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "SmallBallPowerup.h"
#include "Ball.h"
#include "CatapultLeaguePawn.h"
#include "CatapultLeagueGameMode.h"


ASmallBallPowerup::ASmallBallPowerup() : Super() {
	if (SphereComponent != nullptr)
	{
		SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ASmallBallPowerup::OnHit);
	}

	Label->SetText(FText::FromString(TEXT("Small Ball Powerup")));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialGet(TEXT("/Game/VehicleAdv/Materials/MaterialInstances/SolidRed.SolidRed"));
	SphereVisual->CreateDynamicMaterialInstance(0, MaterialGet.Object);
}

void ASmallBallPowerup::BeginPlay() {
	Super::BeginPlay();
}

void ASmallBallPowerup::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr)
	{
		/* if it's a car, we trigger */
		if (OtherActor->IsA(ACatapultLeaguePawn::StaticClass()))
		{
			ACatapultLeagueGameMode* gameMode = Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode());
			ABall* ball = gameMode->GetBall();
			if (ball)
			{
				ball->Shrink();
				SetActorEnableCollision(false);
				this->Destroy();
			}
		}
	}
}



