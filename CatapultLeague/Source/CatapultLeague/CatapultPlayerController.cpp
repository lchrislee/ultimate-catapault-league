// Fill out your copyright notice in the Description page of Project Settings.

#include "CatapultLeague.h"
#include "CatapultPlayerController.h"
#include "CatapultLeaguePawn.h"
#include "CatapultLeagueGameMode.h"

int ACatapultPlayerController::NEXT_PLAYER = 0;

int ACatapultPlayerController::GetNextPlayerNumber()
{
	int next = ACatapultPlayerController::NEXT_PLAYER;
	++ACatapultPlayerController::NEXT_PLAYER;
	if (ACatapultPlayerController::NEXT_PLAYER == 4)
	{
		ACatapultPlayerController::NEXT_PLAYER = 0;
	}
	return next;
}

void ACatapultPlayerController::StartCountdown()
{
	GetWorld()->GetTimerManager().SetTimer(CarryTimer, this, &ACatapultPlayerController::OnCountdownComplete, 5);
}

ACatapultPlayerController::ACatapultPlayerController()
{
	PlayerNumber = GetNextPlayerNumber();
}

void ACatapultPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void ACatapultPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	check(InputComponent);

	if (InputComponent == nullptr)
	{
		return;
	}

	switch (PlayerNumber)
	{
	case 0:
		forward = "MoveForward0";
		right = "MoveRight0";
		shoot = "Shoot0";
		flipCar = "FlipCar0";
		break;
	case 1:
		forward = "MoveForward1";
		right = "MoveRight1";
		shoot = "Shoot1";
		flipCar = "FlipCar1";
		break;
	case 2:
		forward = "MoveForward2";
		right = "MoveRight2";
		shoot = "Shoot2";
		flipCar = "FlipCar2";
		break;
	case 3:
		forward = "MoveForward3";
		right = "MoveRight3";
		shoot = "Shoot3";
		flipCar = "FlipCar3";
		break;
	default:
		return;
	}

	InputComponent->BindAxis(forward, this, &ACatapultPlayerController::MoveForward);
	InputComponent->BindAxis(right, this, &ACatapultPlayerController::MoveRight);
	InputComponent->BindAction(shoot, IE_Pressed, this, &ACatapultPlayerController::OnShootPressed);
	InputComponent->BindAction(flipCar, IE_Pressed, this, &ACatapultPlayerController::FlipCar);
}

void ACatapultPlayerController::OnCountdownComplete()
{
	// TODO Kill Player and launch ball into air.
	ACatapultLeaguePawn* Catapult = Cast<ACatapultLeaguePawn>(GetPawn());
	if (Catapult == nullptr)
	{
		return;
	}
	Catapult->ReleaseBall();
	ACatapultLeagueGameMode* Master = Cast<ACatapultLeagueGameMode>(GetWorld()->GetAuthGameMode());
	Master->RespawnPlayer(this);
}

void ACatapultPlayerController::ResetInputComponent()
{
	InputComponent->AxisBindings.Empty();
	SetupInputComponent();
}

void ACatapultPlayerController::MoveForward(float Val)
{	
	if (IsMoveInputIgnored()) {
		return;
	}

	ACatapultLeaguePawn* Catapult = Cast<ACatapultLeaguePawn>(GetPawn());
	if (Catapult == nullptr)
	{
		return;
	}

	Catapult->MoveForward(Val);
}

void ACatapultPlayerController::MoveRight(float Val)
{
	if (IsMoveInputIgnored()) {
		return;
	}

	ACatapultLeaguePawn* Catapult = Cast<ACatapultLeaguePawn>(GetPawn());
	if (Catapult == nullptr)
	{
		return;
	}

	Catapult->MoveRight(Val);
}

void ACatapultPlayerController::OnShootPressed()
{
	ACatapultLeaguePawn* Catapult = Cast<ACatapultLeaguePawn>(GetPawn());
	if (Catapult == nullptr)
	{
		return;
	}
	Catapult->ShootBall();
	GetWorld()->GetTimerManager().ClearTimer(CarryTimer);
}

void ACatapultPlayerController::FlipCar() {
	ACatapultLeaguePawn* Catapult = Cast<ACatapultLeaguePawn>(GetPawn());
	if (Catapult == nullptr)
	{
		return;
	}
	Catapult->FlipCar();
}
