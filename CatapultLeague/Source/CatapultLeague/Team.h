// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class CATAPULTLEAGUE_API Team
{
public:
	Team(int teamNumber) : TeamNumber(teamNumber) { Score = 0; };
	~Team() {};

	class AGoalPost* GoalPost;
	TArray<class ACatapultPlayerController*> Players;
	TArray<FVector> StartLocations;
	TArray<class APlayerStart*> PlayerStarts;

	int TeamNumber;
	int Score;
};
